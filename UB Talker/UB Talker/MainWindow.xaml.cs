﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;

namespace UB_Talker
{
    using EyeXFramework.Wpf;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string text;
        private int page;
        private SerialPort mySerialPort = new SerialPort("COM4");

        public MainWindow()
        {
            InitializeComponent();
            this.CurrentView.Source = new Uri("/Views/SelectionTest.xaml", UriKind.RelativeOrAbsolute);
            this.page = 0;

            mySerialPort.BaudRate = 115200;
            mySerialPort.Parity = Parity.None;
            mySerialPort.StopBits = StopBits.One;
            mySerialPort.DataBits = 8;
            mySerialPort.WriteLine("REL3.ON");

           


        }

        private void Element_OnHasGazeChanged(object sender, RoutedEventArgs e)
        {
            var label = e.Source as Label;
            if (label == null)
                return;

            if (label.GetHasGaze())
            {
                Console.WriteLine("[MAIN WINDOW] Selected: " + sender);

                if (label == this.CallLight)
                {
                    OnCallButtonSelected();

                }
            }
            
        }

        private void RelayTestButton()
        {

        }

        public string GetMessage()
        {
            return this.text;
        }

        public void SetMessage(string message)
        {
            this.text = message;
        }

        public int GetPage()
        {
            return this.page;
        }
        
        public void SetPage(int page)
        {
            this.page = page;
        }

        private void OnCallButtonSelected()
        {
            Console.WriteLine("Call Light");
            mySerialPort.WriteLine("REL3.OFF");
            System.Threading.Thread.Sleep(500);
            mySerialPort.WriteLine("REL3.ON");
        }
    }
}
