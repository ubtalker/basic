﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UB_Talker.Views
{
    using EyeXFramework.Wpf;
    using System.Speech.Synthesis;

    /// <summary>
    /// Interaction logic for SelectionTest.xaml
    /// </summary>
    public partial class SelectionTest : Page
    {
        private SpeechSynthesizer synth;
        private List<string[]> options;
        private int index;

        public SelectionTest()
        {
            InitializeComponent();

            MainWindow win = Application.Current.MainWindow as MainWindow;
            this.index = win.GetPage();

            synth = new SpeechSynthesizer();
            synth.Volume = 100;
            synth.Rate = -2;

            options = new List<string[]>();
            /*
            options.Add(new string[] { "Pain Meds", "Eye Drops", "Nose Spray", "Clean Eyes" });
            options.Add(new string[] { "Change TV Channel", "Netflix", "Amazon Prime", "Hulu" });
            options.Add(new string[] { "Turn on feeds", "Turn feeds off", "Head of bed up", "Head of bed down" });
            options.Add(new string[] { "Air in cuff", "Ties too tight", "Vent tabes pulling", "" });
            options.Add(new string[] { "Turn light on", "Turn light off", "Open curtain", "Close curtain" });
            options.Add(new string[] { "Clean face", "Chap stick", "", "" });
            options.Add(new string[] { "Call light", "Call Jen", "Text Jen", "" });
            options.Add(new string[] { "Soda in feed tube", "Mouth swab with water", "Mouth swab with mouth wash", "Boost" });
            options.Add(new string[] { "Suction track", "Suction mouth Suction Nose", "Cough assist", "Breathing treatement" });
            options.Add(new string[] { "Need to be changed", "Take off blue slippers", "Put on blue slippers", "Adjust head position" });
            options.Add(new string[] { "Open window", "Close window", "Turn on fan", "Turn off fan" });
            options.Add(new string[] { "Put on sheet", "Take off sheet", "Put on blanket", "Take off blanket" });
            options.Add(new string[] { "Yes", "No", "Maybe", "Ok" });
            options.Add(new string[] { "Up", "Down", "Left", "Right" });
            */
            options.Add(new string[] { "TV channel up", "TV channel down", "TV volume up", "TV volume down" });
            options.Add(new string[] { "Boost", "Adjust body position", "Head of bed up", "Head of bed down" });
            options.Add(new string[] { "Clean face", "Need to be changed", "Put on booties", "Take off booties" });
            options.Add(new string[] { "Chap stick", "Wipe mouth", "Swab with mouth wash", "Swab with water" });
            options.Add(new string[] { "Turn light on", "Turn light off", "Turn on fan", "Turn off fan" });
            options.Add(new string[] { "Open window", "Close window", "Open curtain", "Close curtain" });
            options.Add(new string[] { "Put sheet on", "Take sheet off", "I'm cold", "I'm hot" });
            options.Add(new string[] { "Get letter board", "Text Jen", "Uncover feet", "Perform range of motion" });
            options.Add(new string[] { "Pain meds", "Eye drops", "Nose spray", "Flush foley" });
            options.Add(new string[] { "Feed on", "Feed off", "Soda in tube", "Water in tube" });
            options.Add(new string[] { "Suction mouth", "Suction nose", "Suction track", "Vent tubes pulling" });
            options.Add(new string[] { "Air in cuff", "Ties too tight", "Cough assist", "Breathing treatement" });

            ReloadOptions();
        }

        private void Element_OnHasGazeChanged(object sender, RoutedEventArgs e)
        {
            var label = e.Source as Label;
            if (label == null)
                return;

            if (label.GetHasGaze())
            {
                Console.WriteLine("Selected: " + sender);

                if (label == this.ForwardButton)
                {
                    index++;
                    if (index == this.options.Count)
                        index = 0;

                    ReloadOptions();
                }
                else if (label == this.BackButton)
                {
                    index--;
                    if (index < 0)
                        index = this.options.Count - 1;

                    ReloadOptions();
                }
                else
                {
                    synth.SpeakAsync((label.Content as TextBlock).Text);

                    MainWindow win = Window.GetWindow(this) as MainWindow;
                    win.SetMessage((label.Content as TextBlock).Text);
                    win.SetPage(this.index);

                    NavigationService service = NavigationService.GetNavigationService(this);
                    service.Navigate(new Uri("/Views/MessageView.xaml", UriKind.RelativeOrAbsolute));
                }
            }
        }

        private void ReloadOptions()
        {
            this.Option1.Text = this.options[this.index][0];
            this.Option2.Text = this.options[this.index][1];
            this.Option3.Text = this.options[this.index][2];
            this.Option4.Text = this.options[this.index][3];
        }
    }
}
