﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UB_Talker.Views
{
    using EyeXFramework.Wpf;

    /// <summary>
    /// Interaction logic for MessageView.xaml
    /// </summary>
    public partial class MessageView : Page
    {
        public MessageView()
        {
            InitializeComponent();
            MainWindow win = Application.Current.MainWindow as MainWindow;
            Console.WriteLine("MESSAGE : " + win.GetMessage());
            this.Message.Text = win.GetMessage();
        }

        private void Element_OnHasGazeChanged(object sender, RoutedEventArgs e)
        {
            var label = e.Source as Label;
            if (label == null)
                return;

            if (label.GetHasGaze())
            {
                NavigationService service = NavigationService.GetNavigationService(this);
                service.GoBack();
            }
        }
    }
}
