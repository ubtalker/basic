﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UB_Talker.Views
{
    using EyeXFramework.Wpf;

    /// <summary>
    /// Interaction logic for ButtonTest.xaml
    /// </summary>
    public partial class ButtonTest : Page
    {
        public ButtonTest()
        {
            InitializeComponent();
        }

        private void Element_OnHasGazeChanged(object sender, RoutedEventArgs e)
        {
            var label = e.Source as Label;
            if (label == null)
                return;

            if (label.GetHasGaze())
            {
                Console.WriteLine("Selected: " + sender);
            }
        }
    }
}
