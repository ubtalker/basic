﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UBTalker.Views
{
    /// <summary>
    /// Interaction logic for Buttons.xaml
    /// </summary>
    public partial class Buttons : Page
    {
        public Buttons()
        {
            InitializeComponent();
        }

        private void OnEyeXActivate(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
                return;

            button.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, button));
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Button was clicked!");
        }
    }
}
